package foss.ndts.tudrooms.data;

public interface UniLocation {
    boolean isOk();

    String arrayLengths();

    String displayName();

    String[] buildings();

    String[] names();

    String[] addresses();
}
